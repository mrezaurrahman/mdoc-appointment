var React = require('react');
var ReactDOM = require('react-dom');
var swal = require('sweetalert');



var Journal = React.createClass({
  deleteJournalsFromServer: function() {

  },
    doDelete: function(){
      var journal_id = this.props.journal_id;


      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this journal!",
        type: "warning",   showCancelButton: true,confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true, }, function(){
          $.ajax({
              url: '/journals/'+journal_id,
              dataType: 'json',
              method: 'delete',
              cache: false,
              success: function(data) {
                  this.setState({
                      data: data
                  });
              }.bind(this),
              error: function(xhr, status, err) {
                  console.error(this.props.url, status, err.toString());
              }.bind(this)
          });

            setTimeout(function(){
                swal({   title: "Deleted!",   text: "Your journal has been deleted.",type:"success",   timer: 1000,   showConfirmButton: false });
              }, 2000);
               });



    },
    render: function() {
        var journal_id = this.props.journal_id;
        var journalUrl = "/fulljournal/"+journal_id;
        return ( <div>
            <h2> <a href = {journalUrl} > {this.props.title} </a></h2 >
            <p> <span className = "glyphicon glyphicon-time"> </span> Last updated on {this.props.month} {this.props.day}, {this.props.year} at {this.props.hour}:{this.props.minute}:{this.props.second} {this.props.ampm}</p >
            <p> {
                this.props.children
            } </p>
            <p> < a href = "#"><span className = "glyphicon glyphicon-edit"></span></a>
             &nbsp;
             &nbsp;
             &nbsp;
             &nbsp;
             <a onClick={this.doDelete} href = "#" ><span className = "glyphicon glyphicon-trash"></span></a >
             </p>

            < hr / >
            < /div>

        );
    }
});

var JournalBox = React.createClass({
    loadJournalsFromServer: function() {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({
                    data: data
                });
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },

    getInitialState: function() {
        return {
            data: []
        };
    },
    componentDidMount: function() {
        this.loadJournalsFromServer();
        setInterval(this.loadJournalsFromServer, this.props.pollInterval);
    },
    render: function() {
        return ( <div className = "journalBox">
            <JournalList data = {this.state.data} />  </div >
        );
    }
});

var JournalList = React.createClass({
            render: function() {
                var journalNodes = this.props.data.map(function(journal) {
                    var ampm = "";
                    var updated_time = journal.updated_at;
                    var year = updated_time.substr(0, 4);
                    var month = updated_time.substr(5, 2);

                    switch (month) {
                        case "01":
                            month = "January";
                            break;
                        case "01":
                            month = "February";
                            break;
                        case "03":
                            month = "March";
                            break;
                        case "04":
                            month = "April";
                            break;
                        case "05":
                            month = "May";
                            break;
                        case "06":
                            month = "June";
                            break;
                        case "07":
                            month = "July";
                            break;
                        case "08":
                            month = "August";
                            break;
                        case "09":
                            month = "September";
                            break;
                        case "10":
                            month = "October";
                            break;
                        case "11":
                            month = "November";
                            break;
                        case "12":
                            month = "December";
                            break;
                        default:
                        break;
}

                    var day = updated_time.substr(8, 2);
                    var hour = updated_time.substr(11, 2);
                    //hour = (parseInt(hour)<12)? (updated_time.substr(11,2)+" AM"):((updated_time.substr(11,2)-12)+" PM");
                    if (parseInt(hour) < 12) {
                        hour = updated_time.substr(11, 2);
                        ampm = "AM";
                    } else if (parseInt(hour) == 12) {
                        hour = updated_time.substr(11, 2);
                        ampm = "PM";

                    } else {
                        hour = parseInt(updated_time.substr(11, 2)) - 12;
                        ampm = "PM";
                    }
                    if (parseInt(hour) != 0) {
                        hour = parseInt(hour);
                    } else {
                        hour = "00";
                    }
                    var minute = updated_time.substr(14, 2);
                    var second = updated_time.substr(17, 2);
                    var mainContent = journal.content;
                    var rex = /(<([^>]+)>)/ig;
                    if (mainContent) {
                        if (mainContent.length < 300) {
                            mainContent = (journal.content).replace(rex, "");
                        } else {
                            mainContent = (journal.content.substr(0, 100)).replace(rex, "") + "... ... ...";
                        }


                    } else {
                        mainContent = "";
                    }

      return ( <Journal title = {journal.title} key = {journal._id} journal_id={journal._id} updated_at = {journal.updated_at} year = {year} month = {month}
                        day = {day} hour = {hour} minute = {minute} second = {second} ampm = {ampm} > {mainContent} </Journal>
                    );
                });
                return ( <div className = "journalList"> {
                        journalNodes
                    } </div>);
                }
            });



        ReactDOM.render( < JournalBox url = "/journals"
            pollInterval = {
                2000
            }
            />, document.getElementById('journals') );
