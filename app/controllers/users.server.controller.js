var User = require('mongoose').model('User'),
    passport = require('passport');
var getErrorMessage = function(err) {
    var message = '';
    if (err.code) {
        switch (err.code) {
            case 11000:
            case 11001:
                message = 'Username already exists';
                break;
            default:
                message = 'Something went wrong';
        }
    } else {
        for (var errName in err.errors) {
            if (err.errors[errName].message)
                message = err.errors[errName].message;
        }
    }

    return message;
};

exports.renderLogin = function(req, res, next) {
    if (!req.user) {
        res.render('login', {
            title: 'Log-in Form',
            messages: req.flash('error') || req.flash('info')
        });
    } else {
        return res.redirect('/dashboard');
    }
};

exports.renderRegister = function(req, res, next) {
    if (!req.user) {
        res.render('register', {
            title: 'Register Form',
            messages: req.flash('error')
        });
    } else {
        return res.redirect('/');
    }
};




/*exports.renderProfile = function(req, res, next) {
    if (!req.user) {
        res.render('profile', {
            title: 'Register Form',
            messages: req.flash('error')
        });
    }
    else {
        return res.redirect('/');
    }
};
exports.renderprofile = function(req, res){
   if (!req.user){
        res.render ('profile',{
                    
                    fullname: req.user ? req.user.fullname : '',

                    useremail: req.user ? req.user.email : '',
                    phone: req.user ? req.user.phone : '',
                    
                    });
    
    
   }
    else {
        return res.redirect('/');
    
    }

};*/
exports.register = function(req, res, next) {
    if (!req.user) {
        var user = new User(req.body);
        var message = null;
        user.provider = 'local';
        user.save(function(err) {

            if (err) {
                var message = getErrorMessage(err);
                req.flash('error', message);
                return res.redirect('/login');
            } else
                res.redirect('/login');
        });
    } else {
        return res.redirect('/login');
    }
};

exports.logout = function(req, res) {
    req.logout();
    res.redirect('/');
};




exports.create = function(req, res, next) {
    var user = new User(req.body);
    user.save(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(user);
        }
    });
};

exports.list = function(req, res, next) {
    User.find({}, function(err, users) {
        if (err) {
            return next(err);
        } else {
            res.json(users);
        }
    });
};

exports.read = function(req, res) {
    res.json(req.user);
};

exports.userByID = function(req, res, next, id) {
    User.findOne({
            _id: id
        },
        function(err, user) {
            if (err) {
                return next(err);
            } else {
                req.user = user;
                next();
            }
        }
    );
};


exports.update = function(req, res, next) {
    User.findByIdAndUpdate(req.user.id, req.body, function(err, user) {
        if (err) {
            return next(err);
        } else {
            res.json(user);
        }
    });
};
exports.renderAdd = function(req, res, next) {
    if (req.user) {
        res.render('add_user_info', {
            title: 'User add',
            messages: req.flash('error') || req.flash('info')
        });
    } else {
        return res.redirect('/login');
    }



};

exports.delete = function(req, res, next) {
    req.user.remove(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(req.user);
        }
    })
};


exports.renderAddNewAdmin = function(req, res, next) {
    if (req.user) {
        res.render('AddNewUserAdmin', {

            profile_url: req.user ? req.user.profile_url : '',
            uid: req.user ? req.user.uid : '',
            title: 'User add',
            messages: req.flash('error') || req.flash('info')
        });
    } else {
        return res.redirect('/login');
    }



};





exports.saveOAuthUserProfile = function(req, profile, done) {
    User.findOne({
            email: profile.email
        },
        function(err, user) {
            if (err) {
                //             return done(err);
                console.log(" database err");
            } else {
                if (!user) {

                    var fbUserData = {
                        email: profile.email,
                        provider: profile.provider,
                        providerId: profile.providerId,
                        fullname: profile.fullname,

                    };

                    console.log("new user ! yayyyy");
                    user = new User(fbUserData);
                    user.save(function(err) {
                        if (err) {
                            console.log("user creation failed");
                        }

                        return done(null, user);
                        //return req.res.redirect('/dashboard');




                    });






                } else {
                    console.log("user exists!!");
                    return done(null, user);
                    //return req.res.redirect('/dashboard');

                }
            }
        });
    //     }
    // );
    console.log(profile.providerId);
    console.log(profile.email);
    console.log(profile.fullname);
    console.log(profile.providerData);


    var hello = {
        "moo": "hoo"
    };
    console.log("hello");

};



exports.updateToken = function(id, updatedData) {
    User.findByIdAndUpdate(id, updatedData, function(err, user) {
        if (err) {
            return next(err);
        } else {
            //token update successfull.
        }
    });
};
