var Slot = require('mongoose').model('Slot');
var slotHelper = require('../helpers/slots.server.helper.js');



exports.create = function(req, res, next) {
    var slot = new Slot(req.body);
    //console.log(slot);
    var starttime = slot.startTime;
    var endtime = slot.endTime;



    slotHelper.isSlotAvailable(starttime, endtime);



    slot.save(function(err) {
        if (err) {
            return next(err);
        } else {
            res.json(slot);
        }
    });
};







exports.list = function(req, res, next) {
    Slot.find({}, function(err, slots) {
        if (err) {
            return next(err);
        } else {
            res.json(slots);
        }
    });
};



exports.read = function(req, res) {
    res.json(req.slot);
};
