var mongoose = require('mongoose'),
    crypto = require('crypto'),
    Schema = mongoose.Schema;



var UserSchema = new Schema({
    fullname: String,
    email: {
        type: String,
        trim: true,
        unique: true
    },
    username: String,
    facebookId: String,
    facebookToken: String,
    photoUrl: String,

    password: String,
    confirm_password: String,
    repass: String,
    created_at: {
        type: Date
    },
    updated_at: {
        type: Date
    },
    role: Number,
    is_admin: Number,
    is_rdp: Number,
    is_doctor: Number,
    user_token: String,
    user_secret: String,
    provider: String,
    providerId: String,
    providerData: {}
});

/*
allowed schema
String
Number
Date
Boolean
Buffer
ObjectId
Mixed
Array
*/

UserSchema.pre('save',
    function(next) {
        if (this.password) {
            var md5 = crypto.createHash('md5');
            this.password = md5.update(this.password).digest('hex');
            this.confirm_password = this.password;
        }

        now = new Date();
        this.updated_at = now;
        if (!this.created_at) {
            this.created_at = now;
        }

        this.username = this.email;

        if (this.role == 1) {
            this.is_doctor = 1;
        }
        if (this.role == 2) {
            this.is_rdp = 1;
        }

        this.is_admin = 0;
        if (this.is_doctor == 1) {
            this.is_rdp = 0;
        }
        if (this.is_rdp == 1) {
            this.is_doctor = 0;
        }
        if (this.is_admin == 0) {
            this.is_admin = 0;
        }

        next();
    }
);


UserSchema.methods.authenticate = function(password) {
    var md5 = crypto.createHash('md5');
    md5 = md5.update(password).digest('hex');

    return this.password === md5;
};

UserSchema.statics.findUniqueUsername = function(username, suffix, callback) {
    var _this = this;
    var possibleUsername = username + (suffix || '');

    _this.findOne({
            username: possibleUsername
        },
        function(err, user) {
            if (!err) {
                if (!user) {
                    callback(possibleUsername);
                } else {
                    return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
                }
            } else {
                callback(null);
            }
        }
    );
};


mongoose.model('User', UserSchema);
