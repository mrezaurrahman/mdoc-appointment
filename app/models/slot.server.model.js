var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var SlotSchema = new Schema({
    startTime: Date,
    endTime: Date,
    created_at: {
        type: Date
    },
    updated_at: {
        type: Date
    }
});

/*
allowed schema
String
Number
Date
Boolean
Buffer
ObjectId
Mixed
Array
*/

SlotSchema.pre('save', function(next) {

    now = new Date();
    this.updated_at = now;
    if (!this.created_at) {
        this.created_at = now;
    }

    //this.increment_index = Date.now();
    next();
});



mongoose.model('Slot', SlotSchema);
