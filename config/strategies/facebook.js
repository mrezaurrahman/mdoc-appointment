var passport = require('passport'),
    url = require('url'),
    FacebookStrategy = require('passport-facebook').Strategy,
    config = require('../config'),
    users = require('../../app/controllers/users.server.controller');

module.exports = function() {
    passport.use(new FacebookStrategy({
            clientID: config.facebook.clientID,
            clientSecret: config.facebook.clientSecret,
            callbackURL: config.facebook.callbackURL,
            profileFields: ['emails', 'first_name'],
            passReqToCallback: true
        },
        function(req, accessToken, refreshToken, profile, done) {
            var providerData = profile._json;
            providerData.accessToken = accessToken;
            providerData.refreshToken = refreshToken;
            var nameOfUser = profile.name.givenName + " " + (profile.name.middleName || "") + (profile.name.familyName || "");
            //console.log(profile);
            var providerUserProfile = {
                fullname: nameOfUser,
                email: profile.emails[0].value,
                username: profile.username,
                provider: 'facebook',
                providerId: profile.id,
                providerData: providerData
            };

            users.saveOAuthUserProfile(req, providerUserProfile, done);
        }));
};

// var passport = require('passport'),
//     FacebookStrategy = require('passport-facebook').Strategy,
//     User = require('mongoose').model('User'),
//     url = require('url'),
//     config = require('../config');
//var users = require('../../app/controllers/users.server.controller');


// var facebookConfig = {
//     clientID: config.facebook.clientID,
//     clientSecret: config.facebook.clientSecret,
//     callbackURL: config.facebook.callbackURL,
//     profileFields: ['emails', 'displayName']
// };


// var facebookInit = function(token, refreshToken, profile, callback) {
//     User.findOne({
//         "facebookId": profile.id
//     }, function(err, user) {
//         if (err) return callback(err);

//         if (user) {
//             return callback(null, user);
//         }

//         var newUser = new User();
//         newUser.facebook.id = profile.id;
//         newUser.facebookToken = token;
//         newUser.email = profile.emails[0].value;
//         newUser.fullname = profile.displayName;
//         newUser.photoUrl = 'https://graph.facebook.com/v2.3/' + profile.id + '/picture?type=large';

//         newUser.save(function(err) {
//             if (err) {
//                 throw err;
//             }
//             return callback(null, newUser);
//         });
//     });
// };

// passport.use(new FacebookStrategy(facebookConfig, facebookInit));

// passport.serializeUser(function(user, callback) {
//     callback(null, user.id)
// });

// passport.deserializeUser(function(id, callback) {
//     User.findById(id, function(err, user) {
//         callback(err, user);
//     });
// });

// module.exports = {
//     facebookLogin: passport.authenticate("facebook", {
//         scope: 'email'
//     }),
//     facebookCallback: passport.authenticate("facebook", {
//         successRedirect: "/profile",
//         failureRedirect: "/login"
//     })
// };
