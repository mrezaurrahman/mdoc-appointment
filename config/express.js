var config = require('./config'),
    express = require('express'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    passport = require('passport'),
    flash = require('connect-flash'),
    session = require('express-session');

module.exports = function() {
    var app = express();
    app.use(express.static('./public'));

    // app.use(express.static(path.join(__dirname, 'public')));
    app.use(cookieParser());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.use(bodyParser.json());

    app.use(session({
        saveUninitialized: true,
        resave: true,
        secret: 'OurSuperSecretCookieSecret'
    }));

    app.set('views', './app/views');
    app.set('view engine', 'ejs');


    app.use(flash());
    app.use(passport.initialize());
    app.use(passport.session());

    require('../app/routes/users.server.routes.js')(app);
    require('../app/routes/dashboard.server.routes.js')(app);
    require('../app/routes/index.server.routes.js')(app);
    require('../app/routes/slots.server.routes.js')(app);
    // require('../app/helpers/slots.server.helper.js')(app);






    return app;
};
